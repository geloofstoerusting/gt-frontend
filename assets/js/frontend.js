var framework = {

    addClass: function (element, className) {
        var classes = element.getAttribute('class').split(" ").map(function (string) {
            return string.trim();
        });
        if (classes.indexOf(className) === -1) {
            classes.push(className);
            console.log(classes);
            element.setAttribute('class', classes.join(" "));
        }
    },

    removeClass: function (element, className) {
        var classes = element.getAttribute('class').split(" ").map(function (string) {
            return string.trim();
        });
        if (classes.indexOf(className) !== -1) {
            var index = classes.indexOf(className);
            classes.splice(index, 1);
            element.setAttribute('class', classes.join(" "));
        }
    },

    xhr: {

        get: function (url, callback) {
            var xhr;
            if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
            else {
                var versions = ["MSXML2.XmlHttp.5.0",
                    "MSXML2.XmlHttp.4.0",
                    "MSXML2.XmlHttp.3.0",
                    "MSXML2.XmlHttp.2.0",
                    "Microsoft.XmlHttp"]
                for(var i = 0, len = versions.length; i < len; i++) {
                    try {
                        xhr = new ActiveXObject(versions[i]);
                        break;
                    }
                    catch(e){}
                } // end for
            }
            xhr.onreadystatechange = ensureReadiness;
            function ensureReadiness() {
                if(xhr.readyState < 4) {
                    return;
                }
                if(xhr.status !== 200) {
                    return;
                }
                // all is well
                if(xhr.readyState === 4) {
                    callback(xhr.response);
                }
            }
            xhr.open('GET', url, true);
            xhr.send('');
        }

    }

};

var search = {

    searchBar: undefined,

    searchResources: undefined,

    endpoint: undefined,

    init: function (searchInputId, searchResourceClass, endpoint) {
        search.searchBar = document.getElementById(searchInputId);
        search.searchBar.addEventListener('change', search.doSearch);
        search.searchResources = document.getElementsByClassName(searchResourceClass);
        for (var i = 0;i < search.searchResources.length;i++) {
            search.searchResources[i].addEventListener('change', search.doSearch);
        }
        search.endpoint = endpoint;
    },

    doSearch: function () {
        var searchString = search.searchBar.value;
        var resourceQuery = "";
        for (var i = 0;i < search.searchResources.length;i++) {
            if (search.searchResources[i].checked) {
                resourceQuery += "&resource[]=" + search.searchResources[i].name;
            }
        }
        framework.xhr.get(search.endpoint + "?search="+encodeURI(searchString)+resourceQuery, function (response) {
            document.getElementById('search_result').innerHTML = response;
        });
    }

};

var buttons = document.getElementsByClassName('filter_button');
for (var i = 0; i < buttons.length;i++) {
    buttons[i].addEventListener("click",function(e){
        var attribute = this.getAttribute('data-button-name');
        let checkbox = document.getElementById('input_' + attribute);
        checkbox.click();
        if (checkbox.checked) {
            framework.addClass(this, 'active');
        } else {
            framework.removeClass(this, 'active');
        }
    },false);
}
