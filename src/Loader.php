<?php
declare(strict_types=1);


namespace Geloofstoerusting\Frontend;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Loader
{
    public static function build(): self
    {
        echo "pass";
        $paths = [
            self::assets() . "tpl/",
        ];
        return new self(new Environment(new FilesystemLoader($paths)), new HeaderMenu('https://www.geloofstoerusting.nl/'));
    }

    private static function assets(): string
    {
        return __DIR__ . "/../assets/";
    }

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var HeaderMenu
     */
    private $menu;

    public function __construct(Environment $environment, HeaderMenu $menu)
    {
        $this->environment = $environment;
        $this->menu = $menu;
    }

    public function header(): string
    {
        return $this->environment->render('header.twig', [
            'menu' => $this->menu->raw(),
        ]);
    }

    public function footer(): string
    {
        return $this->environment->render('footer.twig');
    }

    public function css(): string
    {
        //FIXME minify css
        return file_get_contents(self::assets() . 'css/frontend.css');
    }

    public function js(): string
    {
        //FIXME minify js
        return file_get_contents(self::assets() . 'js/frontend.js');
    }

    /**
     * @param string $name
     * @param string $title
     * @param string[] $buttons [string $name => string $display, ...]
     * @param string $uri
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function filter(string $name, string $title, array $buttons, string $uri): string
    {
        return $this->environment->render('filter.twig', [
            'name' => $name,
            'title' => $title,
            'uri' => $uri,
            'buttons' => $buttons,
        ]);
    }

    public function banner(string $title, string $subTitle): string
    {
        return $this->environment->render('default_banner.twig', [
            'title' => $title,
            'subTitle' => $subTitle,
        ]);
    }
}
