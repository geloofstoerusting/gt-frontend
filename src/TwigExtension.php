<?php
declare(strict_types=1);


namespace Geloofstoerusting\Frontend;


use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        $loader = Loader::build();
        $options = [
            'is_safe' => ['html'],
        ];
        return [
            new TwigFunction('gt_header', [$loader, 'header'], $options),
            new TwigFunction('gt_footer', [$loader, 'footer'], $options),
            new TwigFunction('gt_js', [$loader, 'js'], $options),
            new TwigFunction('gt_css', [$loader, 'css'], $options),

            new TwigFunction('gt_filter', [$loader, 'filter'], $options),
            new TwigFunction('gt_banner', [$loader, 'banner'], $options),
        ];
    }
}
