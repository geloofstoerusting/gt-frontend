<?php
declare(strict_types=1);

use Geloofstoerusting\Frontend\Loader;

function gt_loader(): Loader
{
    global $gtLoader;

    if ($gtLoader === null) {
        $gtLoader = Loader::build();
    }

    return $gtLoader;
}

function gt_header(): string
{
    return gt_loader()->header();
}

function gt_footer(): string
{
    return gt_loader()->footer();
}

function gt_css(): string
{
    return gt_loader()->css();
}

function gt_js(): string
{
    return gt_loader()->js();
}

/**
 * @param string $name
 * @param string $title
 * @param string[] $buttons [string $name => string $display, ...]
 * @param string $uri
 * @return string
 * @throws \Twig\Error\LoaderError
 * @throws \Twig\Error\RuntimeError
 * @throws \Twig\Error\SyntaxError
 */
function gt_filter(string $name, string $title, array $buttons, string $uri): string
{
    return gt_loader()->filter($name, $title, $buttons, $uri);
}

function gt_banner(string $title, string $subTitle): string
{
    return gt_loader()->banner($title, $subTitle);
}
