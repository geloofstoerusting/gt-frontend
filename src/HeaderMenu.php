<?php
declare(strict_types=1);


namespace Geloofstoerusting\Frontend;


class HeaderMenu
{
    /**
     * @var string
     */
    private $domain;

    /**
     * HeaderMenu constructor.
     */
    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }

    public function raw(): array
    {
        //FIXME make configurable menu
        return [
            "logo" => "https://www.geloofstoerusting.nl/wp-content/themes/geloofstoerusting/assets/img/logo.png",
            "base" => $this->domain,
            "main_links" => [
                "Home" => $this->domain,
                "Onderwerpen" => $this->domain . "onderwerpen/",
                "Artikelen" => $this->domain . "artikelen/",
                "Video's" => $this->domain . "videos/",
                "Webshop" => "https://shop.geloofstoerusting.nl/",
            ],
            "donate" => [
                "label" => "Help mee!",
                "url" => $this->domain . "doneren/",
            ],
            "more" => [
                [
                    "title" => "Onwankelbare vreugde",
                    "subtitle" => "Dagboek van John Piper",
                    "img" => "TODO",
                ],
                [
                    "title" => "New City Catechismus",
                    "subtitle" => "Bekijk video's en commentaar",
                    "img" => "TODO",
                ],
            ],
        ];
    }
}