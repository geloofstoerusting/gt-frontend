#Frontend Geloofstoerusting

This library contains the common frontend header and footer. The library comes with four functions for getting the following contents:
 - Header
 - Footer
 - Javascript
 - CSS

##Usage

###Blade

When using blade you can use the following functions:
- gt_header()
- gt_footer()
- gt_js()
- gt_css()
```blade
<html>
    <head>
        <script>{{ gt_js() }}</script>
        <style>{{ gt_css() }}</style>
    </head>
    <body>
        {{ gt_header() }}
        YOUR CONTENT HERE
        {{ gt_footer() }}
    </body>
</html>
```

###TWIG

Using twig, add the extension to the twig engine:
```php
/* @var $twig \Twig\Environment */
$twig->addExtension(new \Geloofstoerusting\Frontend\TwigExtension());
```
Now you can use the same functions as described in blade

###PHP

Needing the contents in PHP? Simply build the loader class and call the desired method
```php
<?php
$loader = \Geloofstoerusting\Frontend\Loader::build();
echo <<<HTML
<html>
    <head>
        <script><?= $loader->js() ?></script>
        <style><?= $loader->css() ?></style>
    </head>
    <body>
        <?= $loader->header() ?>
        YOUR CONTENT HERE
        <?= $loader->footer() ?>
    </body>
</html>
HTML
;
```